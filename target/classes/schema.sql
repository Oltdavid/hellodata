CREATE TABLE IF NOT EXISTS employee (
  id VARCHAR(36) PRIMARY KEY,
  fullname VARCHAR(64) NOT NULL,
  email VARCHAR(64) NOT NULL,
  birthdate DATE NOT NULL
);

CREATE INDEX IF NOT EXISTS employee_birthdate ON employee (birthdate);
CREATE INDEX IF NOT EXISTS employee_fullname ON employee (fullname);