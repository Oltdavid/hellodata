package JUnit;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FillData {

	WebDriver driver;

	String baseURL;

	@Before
	public void setUp() throws Exception {
		baseURL = "http://localhost/";
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test
	public void test() throws InterruptedException {
		driver.get(baseURL);
		Thread.sleep(1000);
		driver.findElement(By.xpath("html/body/div[1]/form/button")).click();
		System.out.println("Click Create new Employee");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='fullName']")).sendKeys("Test Name 1");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='emailAddress']")).sendKeys("testname1@test.com");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='birthDate']")).sendKeys("1980", "	", "05", " ", "25");
		Thread.sleep(1000);
		
	}

	@After
	public void tearDown() throws Exception {
		driver.findElement(By.xpath("html/body/div[1]/form/button[1]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("html/body/div[1]/form/button[2]")).click();

		
		// driver.quit();
	}

}
