package DataDrivenTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Utilities.Constans;

public class UsingExcel {
	private WebDriver driver;

	@BeforeClass
	public void setUp() throws Exception {
		driver = new ChromeDriver();

		// Maximize the browser's window
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(Constans.URL);
		
		// Tell the code about the location of Excel file
		ExcelUtility.setExcelFile(Constans.File_Path + Constans.File_Name, "FillData");
	}

	@DataProvider(name = "Data")
	public Object[][] dataProvider() {
		Object[][] testData = ExcelUtility.getTestData("Invalid_Data");
		return testData;
	}

	@Test(dataProvider = "Data")
	public void testUsingExcel(String name, String email) throws Exception {
		driver.findElement(By.xpath("html/body/div[1]/form/button")).click();
		// Enter name
		driver.findElement(By.xpath(".//*[@id='fullName']")).sendKeys(name);

		// Enter email
		driver.findElement(By.xpath(".//*[@id='emailAddress']")).sendKeys(email);

		driver.findElement(By.xpath(".//*[@id='birthDate']")).sendKeys("1980", "	", "05", " ", "25");// birth date
		Thread.sleep(1000);
		driver.findElement(By.xpath("html/body/div[1]/form/button[1]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("html/body/div[1]/form/button[2]")).click();
		driver.findElement(By.xpath("html/body/div[1]/form/button")).click();
		

	}

	@AfterClass
	public void tearDown() throws Exception {
		

		// driver.quit();
	}
}