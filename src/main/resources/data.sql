INSERT INTO employee (id, fullname, email, birthdate) 
VALUES 
('1', 'Smart Guy', 'smart@programmer.net', '1990-08-01'),
('2', 'Cool Guy',  'cool@programmer.net',  '1989-01-01'),
('3', 'Happy Guy', 'happy@programmer.net', '1988-02-01'),
('4', 'Experienced Guy', 'experienced@programmer.net', '1977-02-01');
