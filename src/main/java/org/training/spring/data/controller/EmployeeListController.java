package org.training.spring.data.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.training.spring.data.service.EmployeeService;

/**
 * This controller is responsible for displaying the list of employees - either all of them or those who match a search
 * criteria.
 */
@Controller
public class EmployeeListController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/")
    public String displayEmployeeList(Model model, @RequestParam(required = false) String searchTerm) {

        model.addAttribute("employees", employeeService.searchByNameOrEmail(searchTerm));
        return "employee-list";
    }

}
