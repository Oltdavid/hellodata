package org.training.spring.data.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.training.spring.data.controller.form.EmployeeDetailsForm;
import org.training.spring.data.model.Employee;

@Component
public class EmployeeEntityToEmployeeDetailsFormConverter implements Converter<Employee, EmployeeDetailsForm> {

    @Override
    public EmployeeDetailsForm convert(Employee source) {

        EmployeeDetailsForm e = new EmployeeDetailsForm();

        e.setBirthDate(source.getBirthDate());
        e.setEmailAddress(source.getEmailAddress());
        e.setFullName(source.getFullName());
        e.setId(source.getId());

        return e;
    }

}
