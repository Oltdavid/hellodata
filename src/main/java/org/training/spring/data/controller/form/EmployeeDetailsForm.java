package org.training.spring.data.controller.form;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class EmployeeDetailsForm {

    @Size(max = 36)
    private String id;

    @NotEmpty
    @Size(max = 64)
    private String fullName;

    @NotEmpty
    @Email
    @Size(max = 64)
    private String emailAddress;

    @NotNull
    private Date birthDate;

    @NotNull
    private Action action;

    public static enum Action {
        SAVE, BACK, DELETE
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "EmployeeDetailsForm [id=" + id + ", fullName=" + fullName + ", emailAddress=" + emailAddress
                + ", birthDate=" + birthDate + ", action=" + action + "]";
    }

}
