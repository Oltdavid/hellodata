package org.training.spring.data.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.training.spring.data.controller.form.EmployeeDetailsForm;
import org.training.spring.data.model.Employee;
import org.training.spring.data.repository.EmployeeRepository;

/**
 * This controller is responsible for displaying, editing, creating or deleting individual employee data.
 */
@Controller
@RequestMapping("/employee-details.html")
public class EmployeeDetailsController {

    private static final String VIEW_EMPLOYEE_DETAILS = "employee-details";

    @Autowired
    private EmployeeRepository employeeRepository;

    @Resource
    private ConversionService conversionService;

    @RequestMapping(method = RequestMethod.GET)
    public String displayEmployee(Model model, @RequestParam(required = false) String employeeId) {

        Employee employee = employeeId != null ? employeeRepository.getById(employeeId) : new Employee();

        EmployeeDetailsForm form = conversionService.convert(employee,
                EmployeeDetailsForm.class);

        model.addAttribute("employeeForm", form);
        return VIEW_EMPLOYEE_DETAILS;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createOrUpdateEmployee(@ModelAttribute("employeeForm") @Valid EmployeeDetailsForm employeeForm,
            BindingResult bindingResult) {

        switch (employeeForm.getAction()) {

        case SAVE:

            if (bindingResult.hasErrors()) { // if a binding error happened send back the user the form for correction
                bindingResult.reject("employeeDetails.error.userInput");
                return VIEW_EMPLOYEE_DETAILS;
            }

            Employee employee = conversionService.convert(employeeForm, Employee.class);

            if (employeeForm.getId() == null) {
                employeeRepository.create(employee);
            } else {
                employeeRepository.update(employee);
            }
            break;

        case DELETE:
            employeeRepository.deleteById(employeeForm.getId());
            break;
        }

        return "redirect:/";

    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleNotExistingEmployee(EmptyResultDataAccessException e) {
        return "error/employee-does-not-exist";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true /* empty as null */));
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true /* allow empty */));
    }

}
