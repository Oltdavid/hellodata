package org.training.spring.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloDataApplication.class, args);
    }
}
//proba