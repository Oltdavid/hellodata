package org.training.spring.data.model;

import java.util.Date;

public class Employee {

    private String id;
    private String fullName;
    private String emailAddress;
    private Date birthDate;

    public Employee() {

    }

    public Employee(String id, String fullName, String emailAddress, Date birthDate) {
        this.id = id;
        this.fullName = fullName;
        this.emailAddress = emailAddress;
        this.birthDate = birthDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", fullName=" + fullName + ", emailAddress=" + emailAddress + ", birthDate="
                + birthDate + "]";
    }

}
