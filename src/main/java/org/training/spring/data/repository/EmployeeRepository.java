package org.training.spring.data.repository;

import java.time.LocalDate;
import java.util.List;

import org.training.spring.data.model.Employee;

public interface EmployeeRepository extends CRUDRepository<Employee, String> {

    List<Employee> getEmployeesBornAfter(LocalDate earliestBirthDate);

    List<Employee> findByNameOrEmailIgnoreCaseContaining(String s);
}
