package org.training.spring.data.repository.impl;

import static org.springframework.util.Assert.notNull;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.training.spring.data.model.Employee;
import org.training.spring.data.repository.EmployeeRepository;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Employee> rowMapper = new RowMapper<Employee>() {
        public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
            Employee e = new Employee();
            e.setId(rs.getString("id"));
            e.setFullName(rs.getString("fullName"));
            e.setEmailAddress(rs.getString("email"));
            e.setBirthDate(rs.getDate("birthDate"));
            return e;
        }
    };

    public List<Employee> getEmployeesBornAfter(LocalDate earliestBirthDate) {

        notNull(earliestBirthDate, "The earliestBirthDate can not be null");

        return jdbcTemplate.query(
                "SELECT id, fullName, email, birthdate FROM Employee where birthDate >= ? ORDER BY birthDate",
                new Object[] { Date.valueOf(earliestBirthDate) }, rowMapper);

    }

    public List<Employee> findAll() {
        return jdbcTemplate.query("SELECT id, fullName, email, birthdate FROM Employee ORDER BY fullName",
                new Object[] {}, rowMapper);
    }

    @Override
    public void create(Employee e) {

        notNull(e, "The entity can not be null");

        if (e.getId() == null) {
            e.setId(UUID.randomUUID().toString());
        }
        jdbcTemplate.update("INSERT INTO employee (id, fullName, email, birthdate) VALUES (?, ?, ?, ?)", e.getId(),
                e.getFullName(), e.getEmailAddress(), e.getBirthDate());
    }

    @Override
    public Employee getById(String id) { // may throw EmptyResultDataAccessException or
                                         // IncorrectResultSizeDataAccessException
        notNull(id, "The id can not be null");

        Employee e = jdbcTemplate.queryForObject("SELECT id, fullName, email, birthdate FROM Employee where id = ?",
                new Object[] { id }, rowMapper);

        if (e == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return e;
    }

    @Override
    public void update(Employee e) {
        notNull(e, "The entity can not be null");
        jdbcTemplate.update("UPDATE employee set fullName = ?,  email = ?, birthdate = ? where id = ?", e.getFullName(),
                e.getEmailAddress(), e.getBirthDate(), e.getId());
    }

    @Override
    public void deleteById(String id) {
        notNull(id, "The id can not be null");
        jdbcTemplate.update("DELETE FROM employee where id = ?", id);
    }

    @Override
    public List<Employee> findByNameOrEmailIgnoreCaseContaining(String s) {
        notNull(s, "Search string can not be null");
        String pattern = ("%" + s + "%").toLowerCase();
        return jdbcTemplate.query(
                "SELECT id, fullName, email, birthdate FROM Employee"
                        + " WHERE LOWER(fullName) LIKE ? OR LOWER(email) LIKE ?"
                        + " ORDER BY fullName",
                new Object[] { pattern, pattern }, rowMapper);
    }
}