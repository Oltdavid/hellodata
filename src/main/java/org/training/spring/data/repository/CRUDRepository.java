package org.training.spring.data.repository;

import java.util.List;

public interface CRUDRepository<E, K> {

    public void create(E e);

    public E getById(K id);

    public List<E> findAll();

    public void update(E e);

    public void deleteById(K id);
}
