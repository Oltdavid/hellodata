package org.training.spring.data.service;

import java.util.List;

import org.training.spring.data.model.Employee;

public interface EmployeeService {

    /**
     * Returns the list of employees, whose name or email address contains the specified search term. The order of the
     * employees are by the name.
     * 
     * @param searchTerm
     *            the string to search in the employees name or email address
     * @return the list of matching employees
     */
    List<Employee> searchByNameOrEmail(String searchTerm);

}
