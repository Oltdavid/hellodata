package org.training.spring.data.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.training.spring.data.model.Employee;
import org.training.spring.data.repository.EmployeeRepository;
import org.training.spring.data.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> searchByNameOrEmail(String searchTerm) {

        List<Employee> employeeResult = null;

        if (searchTerm == null) {
            employeeResult = employeeRepository.findAll();
        } else {
            searchTerm = searchTerm.trim();
            employeeResult = searchTerm.length() == 0 ? employeeRepository.findAll()
                    : employeeRepository.findByNameOrEmailIgnoreCaseContaining(searchTerm);
        }

        return employeeResult;
    }

    @Transactional
    public void replaceEmailAddressDomain(String newDomain, String... employeeIds) {

        Assert.notNull(newDomain);
        Assert.hasText(newDomain);
        Assert.notEmpty(employeeIds);

        newDomain = "@" + newDomain;
        boolean stop = false;

        for (String id : employeeIds) {
            Employee e = employeeRepository.getById(id);
            String email = e.getEmailAddress();
            email = email.replaceAll("@.+", newDomain);

            e.setEmailAddress(email);
            employeeRepository.update(e);

        }
    }

}
